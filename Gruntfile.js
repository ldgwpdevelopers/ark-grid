module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        // CSS processing
        sass: {
            dist: {
                files: {
                    'assets/css/source/ark/ark.css' : 'assets/sass/ark/ark.scss'
                }
            },
            merged_example: {
                files: {
                    'assets/css/source/style.css' : 'assets/sass/style.scss'
                }
            }
        },
        combine_mq: {
            dist: {
                expand: true,
                cwd: 'assets/css/source/ark',
                src: 'ark.css',
                dest: 'assets/css/source/ark'
            },
            merged_example: {
                expand: true,
                cwd: 'assets/css/source',
                src: 'style.css',
                dest: 'assets/css/source'
            }
        },
        cssmin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: 'assets/css/source/ark',
                    src: 'ark.css',
                    dest: 'assets/css/ark',
                    ext: '.min.css'
                }]
            },
            merged_example: {
                files: [{
                    expand: true,
                    cwd: 'assets/css/source',
                    src: 'style.css',
                    dest: 'assets/css',
                    ext: '.min.css'
                }]
            }
        },
        // Watch
        watch: {
            css: {
                files: 'assets/sass/**/*.scss',
                tasks: ['sass', 'combine_mq', 'cssmin']
            }
        }
    });
    // style.min.css Processing
    grunt.loadNpmTasks('grunt-contrib-sass'); // Compile sass
    grunt.loadNpmTasks('grunt-combine-mq'); // Combine media queries
    grunt.loadNpmTasks('grunt-contrib-cssmin'); // Minimize file
    // Watch
    grunt.loadNpmTasks('grunt-contrib-watch');
};