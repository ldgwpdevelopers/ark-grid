# README #

Ark Grid is a flexbox grid system that allows you to easily implement a grid and achieve the layout you want without having to write too much additional CSS.

### What is this repository for? ###

This repository is for keeping track of the changes made to the code of the Ark Grid System.

Version 1.0

### How do I get set up? ###

Set Up

To install this grid system just clone the repository.

If you want you can use the included dist version including it as a stylesheet on your file's header. You can find it on assets/css/ark/ark.min.css

You can also include the grid with your sass files so it all gets in one stylesheet by adding the assets/sass/ark folder to your sass folder and importing the ark main file with @import "ark/ark.scss";

Configuration

To edit any of the grid breakpoints, edit the vars file.
To add support for a new viewport size screen on any of the provided grid functions, check the code on grid file.
To add support for a new viewport size screen on any of the provided alignement functions, check the code on the alignements file.

Dependencies

The following libraries are required if you want to build your own custom config with this grid (just clone repo and run npm install):

* Grunt
* Grunt Sass
* Grunt CSSMin
* Grunt Combine-MQ

If you dont want to modify anything you can always download the assets/css/ark/ark.min.css file and include it on your project.

### Who do I talk to? ###

wpdev@lobodeguerra.net